PREFIX = /usr/local

bibelen: bibelen.sh bibelen.awk bibelen.tsv
	cat bibelen.sh > $@
	echo 'exit 0' >> $@
	echo '#EOF' >> $@
	tar cz bibelen.awk bibelen.tsv >> $@
	chmod +x $@

test: bibelen.sh
	shellcheck -s sh bibelen.sh

clean:
	rm -f bibelen

install: bibelen
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f bibelen $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/bibelen

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/bibelen

.PHONY: test clean install uninstall
